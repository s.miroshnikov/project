let Name, Email, Comment, Phone;

function getValue(){
    Name = localStorage.getItem('nameField');
    Email = localStorage.getItem('emailField');
    Comment = localStorage.getItem('commentField');
    Phone = localStorage.getItem('phoneField');

    console.log(Name);
    console.log(Email);
    console.log(Comment);
    if (Name !==undefined && Email !==undefined && Comment!==undefined && Phone!==undefined) {
        document.getElementById("nameField").value = Name;
        document.getElementById("phoneNumber").value = Phone;
        document.getElementById("emailField").value = Email;
        document.getElementById("commentField").value = Comment;
    }else {
        document.getElementById("nameField").value = "";
        document.getElementById("emailField").value = "";
        document.getElementById("phoneNumber").value = "";
        document.getElementById("commentField").value = "";
    }
}

window.addEventListener('popstate', function(){
    localStorage.setItem('nameField', document.getElementById("nameField").value);
    localStorage.setItem('emailField', document.getElementById("emailField").value);
    localStorage.setItem('commentField', document.getElementById("commentField").value);
    localStorage.setItem('phoneField', document.getElementById("phoneField").value);
});
